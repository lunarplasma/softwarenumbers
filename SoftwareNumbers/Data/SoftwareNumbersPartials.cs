﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;

namespace SoftwareNumbers.Data
{    
    public partial class Version
    {
        public Version(short? majorVersion, short? minorVersion, short? bugFixVersion)
        {
            this.MajorVersion = majorVersion ?? 0;
            this.MinorVersion = minorVersion ?? 0;
            this.BugfixVersion = bugFixVersion ?? 0;
        }

        public Version()
        {
        }

        public override string ToString()
        {
            string _MajorVersion;
            string _MinorVersion;
            string _BugfixVersion;

            _MajorVersion = MajorVersion.ToString();
            _MinorVersion = MinorVersion.ToString();

            string versionStr = _MajorVersion;
            versionStr = versionStr + ConfigVariables.versionSeparator + _MinorVersion;

            if (ConfigVariables.usingBugFixVersion == true)
            {
                _BugfixVersion = BugfixVersion.ToString();
                if (BugfixVersion != null)
                {
                    versionStr = versionStr + ConfigVariables.versionSeparator + _BugfixVersion;
                }
            }

            return versionStr;
        }
    }

    public partial class SW_SR
    {
        // This version appears to be inefficient because it performs a query per value
        // TODO: Find a better way!
        public string LatestVersion 
        {
            get
            {
                if (this.eVersions.Count() > 0)
                {
                    // latestVerSelect properties: maxVer, MajorVersion, MinorVersion, Bugfix Version
                    var latestVerSelect = eVersions.Select(x => new { maxVer = (x.Version.MajorVersion * 1000000 + x.Version.MinorVersion + 1000 + (x.Version.BugfixVersion == null ? 0 : x.Version.BugfixVersion)), x.Version.MajorVersion, x.Version.MinorVersion, x.Version.BugfixVersion });
                    var maxVer = latestVerSelect.OrderByDescending(c => c.maxVer).FirstOrDefault();

                    string _MajorVersion;
                    string _MinorVersion;
                    string _BugfixVersion;
                    string versionStr;

                    _MajorVersion = maxVer.MajorVersion.ToString();
                    _MinorVersion = maxVer.MinorVersion.ToString();

                    versionStr = _MajorVersion;
                    versionStr = versionStr + ConfigVariables.versionSeparator + _MinorVersion;

                    if (ConfigVariables.usingBugFixVersion == true)
                    {
                        _BugfixVersion = maxVer.BugfixVersion.ToString();
                        if (maxVer.BugfixVersion != null)
                        {
                            versionStr = versionStr + ConfigVariables.versionSeparator + _BugfixVersion;
                        }
                    }


                    return versionStr;
                }
                else return "";
            }
        }
    }

    public partial class SW_SV
    {
        public List<short?> ENGList
        {
            get
            {
                List<short?> engIDs = new List<short?> { ENG0, ENG1, ENG2, ENG3, ENG4, ENG5, ENG6, ENG7 };
                return engIDs;
            }
        }
    }
}

