
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/25/2014 13:58:24
-- Generated from EDMX file: C:\Users\john.gosoco\Dropbox\Coding\Visual Studio\SoftwareNumbers\SoftwareNumbers\SoftwareNumbers.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE localdb1;
-- USE s1kConnectionString;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_SRtoSV]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SV] DROP CONSTRAINT [FK_SRtoSV];
GO
IF OBJECT_ID(N'[dbo].[FK_SVtoSVE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SVE] DROP CONSTRAINT [FK_SVtoSVE];
GO
IF OBJECT_ID(N'[dbo].[FK_SVtoSVC]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SVC] DROP CONSTRAINT [FK_SVtoSVC];
GO
IF OBJECT_ID(N'[dbo].[FK_SRtoSRT]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SRT] DROP CONSTRAINT [FK_SRtoSRT];
GO
IF OBJECT_ID(N'[dbo].[FK_lstCompiler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SVC] DROP CONSTRAINT [FK_lstCompiler];
GO
IF OBJECT_ID(N'[dbo].[FK_lstIDE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SVC] DROP CONSTRAINT [FK_lstIDE];
GO
IF OBJECT_ID(N'[dbo].[FK_SW_SVCSW_lstProcessor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SW_SVC] DROP CONSTRAINT [FK_SW_SVCSW_lstProcessor];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[SW_SR]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_SR];
GO
IF OBJECT_ID(N'[dbo].[SW_SV]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_SV];
GO
IF OBJECT_ID(N'[dbo].[SW_SVE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_SVE];
GO
IF OBJECT_ID(N'[dbo].[SW_SVC]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_SVC];
GO
IF OBJECT_ID(N'[dbo].[SW_SRT]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_SRT];
GO
IF OBJECT_ID(N'[dbo].[SW_lstCompiler]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_lstCompiler];
GO
IF OBJECT_ID(N'[dbo].[SW_lstIDE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_lstIDE];
GO
IF OBJECT_ID(N'[dbo].[SW_lstProcessor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SW_lstProcessor];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'SW_SR'
CREATE TABLE [dbo].[SW_SR] (
    [SR_SoftwareNumber] smallint IDENTITY(1,1) NOT NULL,
    [SR_Description] nvarchar(max)  NOT NULL,
    [SR_CreationDate] datetime  NOT NULL
);
GO

-- Creating table 'SW_SV'
CREATE TABLE [dbo].[SW_SV] (
    [SV_Index] int IDENTITY(1,1) NOT NULL,
    [SV_SoftwareNumber] smallint  NOT NULL,
    [MajorVersion] smallint  NOT NULL,
    [MinorVersion] smallint  NOT NULL,
    [BugfixVersion] smallint  NOT NULL,
    [SV_ReleaseDate] datetime  NOT NULL,
    [ChangeLog] nvarchar(max)  NULL,
    [CustChangeLog] nvarchar(max)  NULL
);
GO

-- Creating table 'SW_SVE'
CREATE TABLE [dbo].[SW_SVE] (
    [SVE_Index] int IDENTITY(1,1) NOT NULL,
    [ENG0] smallint  NOT NULL,
    [ENG1] smallint  NULL,
    [ENG2] smallint  NULL,
    [ENG3] smallint  NULL,
    [ENG4] smallint  NULL,
    [ENG5] smallint  NULL,
    [ENG6] smallint  NULL,
    [ENG7] smallint  NULL
);
GO

-- Creating table 'SW_SVC'
CREATE TABLE [dbo].[SW_SVC] (
    [SVC_Index] int IDENTITY(1,1) NOT NULL,
    [Compiler] smallint  NOT NULL,
    [IDE] smallint  NOT NULL,
    [Processor] smallint  NOT NULL
);
GO

-- Creating table 'SW_SRT'
CREATE TABLE [dbo].[SW_SRT] (
    [SRT_SoftwareNumber] smallint  NOT NULL,
    [SRT_Tag] smallint  NOT NULL,
    [SRT_Key] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SW_lstCompiler'
CREATE TABLE [dbo].[SW_lstCompiler] (
    [ID_Compiler] smallint IDENTITY(1,1) NOT NULL,
    [CompilerName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SW_lstIDE'
CREATE TABLE [dbo].[SW_lstIDE] (
    [ID_IDE] smallint IDENTITY(1,1) NOT NULL,
    [IDEName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SW_lstProcessor'
CREATE TABLE [dbo].[SW_lstProcessor] (
    [ID_Processor] smallint IDENTITY(1,1) NOT NULL,
    [warehouse] nvarchar(max)  NOT NULL,
    [product_code] nvarchar(max)  NOT NULL,
    [part_number] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [SR_SoftwareNumber] in table 'SW_SR'
ALTER TABLE [dbo].[SW_SR]
ADD CONSTRAINT [PK_SW_SR]
    PRIMARY KEY CLUSTERED ([SR_SoftwareNumber] ASC);
GO

-- Creating primary key on [SV_Index] in table 'SW_SV'
ALTER TABLE [dbo].[SW_SV]
ADD CONSTRAINT [PK_SW_SV]
    PRIMARY KEY CLUSTERED ([SV_Index] ASC);
GO

-- Creating primary key on [SVE_Index] in table 'SW_SVE'
ALTER TABLE [dbo].[SW_SVE]
ADD CONSTRAINT [PK_SW_SVE]
    PRIMARY KEY CLUSTERED ([SVE_Index] ASC);
GO

-- Creating primary key on [SVC_Index] in table 'SW_SVC'
ALTER TABLE [dbo].[SW_SVC]
ADD CONSTRAINT [PK_SW_SVC]
    PRIMARY KEY CLUSTERED ([SVC_Index] ASC);
GO

-- Creating primary key on [SRT_Key], [SRT_SoftwareNumber] in table 'SW_SRT'
ALTER TABLE [dbo].[SW_SRT]
ADD CONSTRAINT [PK_SW_SRT]
    PRIMARY KEY CLUSTERED ([SRT_Key], [SRT_SoftwareNumber] ASC);
GO

-- Creating primary key on [ID_Compiler] in table 'SW_lstCompiler'
ALTER TABLE [dbo].[SW_lstCompiler]
ADD CONSTRAINT [PK_SW_lstCompiler]
    PRIMARY KEY CLUSTERED ([ID_Compiler] ASC);
GO

-- Creating primary key on [ID_IDE] in table 'SW_lstIDE'
ALTER TABLE [dbo].[SW_lstIDE]
ADD CONSTRAINT [PK_SW_lstIDE]
    PRIMARY KEY CLUSTERED ([ID_IDE] ASC);
GO

-- Creating primary key on [ID_Processor] in table 'SW_lstProcessor'
ALTER TABLE [dbo].[SW_lstProcessor]
ADD CONSTRAINT [PK_SW_lstProcessor]
    PRIMARY KEY CLUSTERED ([ID_Processor] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [SV_SoftwareNumber] in table 'SW_SV'
ALTER TABLE [dbo].[SW_SV]
ADD CONSTRAINT [FK_SRtoSV]
    FOREIGN KEY ([SV_SoftwareNumber])
    REFERENCES [dbo].[SW_SR]
        ([SR_SoftwareNumber])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SRtoSV'
CREATE INDEX [IX_FK_SRtoSV]
ON [dbo].[SW_SV]
    ([SV_SoftwareNumber]);
GO

-- Creating foreign key on [SVE_Index] in table 'SW_SVE'
ALTER TABLE [dbo].[SW_SVE]
ADD CONSTRAINT [FK_SVtoSVE]
    FOREIGN KEY ([SVE_Index])
    REFERENCES [dbo].[SW_SV]
        ([SV_Index])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SVC_Index] in table 'SW_SVC'
ALTER TABLE [dbo].[SW_SVC]
ADD CONSTRAINT [FK_SVtoSVC]
    FOREIGN KEY ([SVC_Index])
    REFERENCES [dbo].[SW_SV]
        ([SV_Index])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SRT_SoftwareNumber] in table 'SW_SRT'
ALTER TABLE [dbo].[SW_SRT]
ADD CONSTRAINT [FK_SRtoSRT]
    FOREIGN KEY ([SRT_SoftwareNumber])
    REFERENCES [dbo].[SW_SR]
        ([SR_SoftwareNumber])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SRtoSRT'
CREATE INDEX [IX_FK_SRtoSRT]
ON [dbo].[SW_SRT]
    ([SRT_SoftwareNumber]);
GO

-- Creating foreign key on [Compiler] in table 'SW_SVC'
ALTER TABLE [dbo].[SW_SVC]
ADD CONSTRAINT [FK_lstCompiler]
    FOREIGN KEY ([Compiler])
    REFERENCES [dbo].[SW_lstCompiler]
        ([ID_Compiler])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_lstCompiler'
CREATE INDEX [IX_FK_lstCompiler]
ON [dbo].[SW_SVC]
    ([Compiler]);
GO

-- Creating foreign key on [IDE] in table 'SW_SVC'
ALTER TABLE [dbo].[SW_SVC]
ADD CONSTRAINT [FK_lstIDE]
    FOREIGN KEY ([IDE])
    REFERENCES [dbo].[SW_lstIDE]
        ([ID_IDE])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_lstIDE'
CREATE INDEX [IX_FK_lstIDE]
ON [dbo].[SW_SVC]
    ([IDE]);
GO

-- Creating foreign key on [IDE] in table 'SW_SVC'
ALTER TABLE [dbo].[SW_SVC]
ADD CONSTRAINT [FK_SW_SVCSW_lstProcessor]
    FOREIGN KEY ([IDE])
    REFERENCES [dbo].[SW_lstProcessor]
        ([ID_Processor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SW_SVCSW_lstProcessor'
CREATE INDEX [IX_FK_SW_SVCSW_lstProcessor]
ON [dbo].[SW_SVC]
    ([IDE]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------