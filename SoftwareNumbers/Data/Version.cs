//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftwareNumbers.Data
{
    using System;
    
    public partial class Version
    {
        public short MajorVersion { get; set; }
        public short MinorVersion { get; set; }
        public Nullable<short> BugfixVersion { get; set; }
    }
}
