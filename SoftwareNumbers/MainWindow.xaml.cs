﻿using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Input;
using SoftwareNumbers.Data;
using SoftwareNumbers.Model;
using SoftwareNumbers.Helpers;
using System.Windows.Controls;

namespace SoftwareNumbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {
        private SoftwareNumbersModel MainSoftwareNumbersModel = new SoftwareNumbersModel();
        private RegistryModel _reg = new RegistryModel();

        //public CollectionViewSource SR_ViewSource = new CollectionViewSource();

        public MainWindow()  
        {
            InitializeComponent();
            DataContext = _reg;      
        }

        //I don't need this; I've defined it in the xaml to be use the Save Command
        //private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    //MainSoftwareNumbersModel.SaveChanges();
        //    _reg.Save();
        //    this.SRDataGrid.Items.Refresh();
        //}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.MainSoftwareNumbersModel.Dispose();
        }

        private void FontSizePopup_MouseLeave(object sender, MouseEventArgs e)
        { 
            //FontSizePopup.IsOpen = false;
        }

        private void MenuItem_FontSize_Click(object sender, RoutedEventArgs e) 
        {
            FontSizePopup.IsOpen = !FontSizePopup.IsOpen;
        }

        private void ShowAddNewPopup(object sender, RoutedEventArgs e)
        {
            AddNewNumberWindow addnewWindow = new AddNewNumberWindow(ref _reg);
            addnewWindow.Show();
        }

        private void SRDataGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            // test if left CTRL key is pressed. If it is, change the sldr value to change font size                        
            bool CtrlTest = (Keyboard.IsKeyDown(Key.LeftCtrl));        

            if ((e.Delta > 0) && CtrlTest)
            {
                SRDataGrid.CaptureMouse();
                sldr.Value += 0.5;
            }
            else if ((e.Delta < 0) && CtrlTest)
            {
                SRDataGrid.CaptureMouse();
                sldr.Value -= 0.5;
            }
            SRDataGrid.Focus();
        }

        private void ShowVersions_click(object sender, RoutedEventArgs e)
        {
            // get selected record from the SRDataGrid
            SW_SR current = (SW_SR)SRDataGrid.CurrentItem;

            SoftwareVersionsWindow svWindow = new SoftwareVersionsWindow(ref _reg, current.SR_SoftwareNumber);            
            svWindow.Show();
        }

        private void SearchBox1_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            string txtstring = ((TextBox)sender).Text;
            _reg.FilterNumbers(txtstring);
        }
    }
}
