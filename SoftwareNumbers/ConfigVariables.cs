﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareNumbers
{
    public static class ConfigVariables
    {
        public const bool usingBugFixVersion = false;
        public const string versionSeparator = "v";
        public const short engineeringDepartment = 1;
        public const string NEWDESC_DEFAULT = "Enter new description here...";
    }
}
