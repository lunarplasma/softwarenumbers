﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Windows.Data;
using System.Windows;
using SoftwareNumbers.Data;

namespace SoftwareNumbers
{
    public class BoolToVisibilityConverter : IValueConverter
        {

            #region IValueConverter Members

            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                System.Diagnostics.Debug.Assert(targetType == typeof(Visibility));
                try
                {
                    bool b = (bool)value;
                    if (b) return Visibility.Visible;
                }
                catch { }
                return Visibility.Collapsed;
            }

            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }

            #endregion
        }

    public class VersionToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                Data.Version v = (Data.Version)value;
                string _MajorVersion;
                string _MinorVersion;
                string _BugfixVersion;

                _MajorVersion = v.ToString();
                _MinorVersion = v.ToString();

                string version = _MajorVersion;
                version = version + "." + _MinorVersion;

                _BugfixVersion = v.BugfixVersion.ToString();
                if (v.BugfixVersion != null)
                {
                    version = version + "." + _BugfixVersion;
                }

                return version;
            }
            catch { }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int src = (int)value;
                Data.Version v = new Data.Version();
                v.MajorVersion  = (short)(src / 1000000);
                v.MinorVersion  = (short)((src / 1000) % 1000);
                v.BugfixVersion = (short?)(src / 1000000);
                return v;
            }
            catch { }
            return 0;
        }
    }

}
