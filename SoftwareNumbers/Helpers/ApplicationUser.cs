﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SoftwareNumbers.Model;
using SoftwareNumbers.Data;

namespace SoftwareNumbers.Helpers
{
    public class ApplicationUser
    {
        private string _userEName = "";
        private string _login = "";
        private string _longName = "";
        private string _initials = "";
        private short? _department = 0;


        /// Declaration
        public ApplicationUser(RegistryModel Registry)
        {
            UsersModel userlist = new UsersModel(Registry);
            _userEName = Environment.UserName;
            User_List thisUser = userlist.GetSpecificUser(_userEName);
            if (thisUser == null)
            {
                thisUser = new User_List();
                thisUser.login = _userEName;
                thisUser.longname = _userEName;
                thisUser.longname = "none";
            }
            else
            {
                _login = thisUser.login;
                _longName = thisUser.longname;
                _initials = thisUser.initials;
                _department = thisUser.department;
            }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string LongName
        {
            get { return _longName; }
            set { _longName = value; }
        }

        public string Initials
        {
            get { return _initials; }
            set { _initials = value; }
        }

        public short? Department
        {
            get { return _department; }
            set { _department = value; }
        }
    }
}
