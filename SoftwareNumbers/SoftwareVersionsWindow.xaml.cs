﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SoftwareNumbers.Data;
using SoftwareNumbers.Model;
using SoftwareNumbers.Helpers;
using System.ComponentModel;

namespace SoftwareNumbers
{   
    /// <summary>
    /// Interaction logic for SoftwareVersionsWindow.xaml
    /// </summary>
    public partial class SoftwareVersionsWindow : Window
    {
        private short _softwarenumber;
        private RegistryModel _reg;
        public ApplicationUser user;
        ICollectionView source;

 

        public SoftwareVersionsWindow(ref RegistryModel registryContext, short softwareNumber)
        {
            InitializeComponent();

            this._reg = registryContext;
            this._softwarenumber = softwareNumber;

            //source = CollectionViewSource.GetDefaultView(_reg.GetVersions);
            source = registryContext.GetVersions;
            source.Filter = item =>
            {
                SW_SV record = item as SW_SV;
                return record.SV_SoftwareNumber == softwareNumber;
            };

            SV_Datagrid.ItemsSource = source;
            user = new ApplicationUser(_reg);   //initialize the user
            this.SV_TitleBlock.Text = "Viewing Software Number e" + _softwarenumber.ToString("000") + " and you are:" + user.Login;            
        }

        public void AddNewMajorRevision(object sender, RoutedEventArgs e)
        {
            Data.Version maxVersion = _reg.GetMaxVersion(_softwarenumber);
            short newMajorVersion = maxVersion == null ? (short)0 : (short)(maxVersion.MajorVersion + 1);

            AddNewVersionWindow newWindow = new AddNewVersionWindow(_softwarenumber, newMajorVersion, (short)0, ref _reg);
            newWindow.Show();
        }
        
        public void AddNewMinorRevision(object sender, RoutedEventArgs e)
        {
            Data.Version maxVersion = _reg.GetMaxVersion(_softwarenumber);
            short newMajorVersion = maxVersion == null ? (short)0 : (short)(maxVersion.MajorVersion);
            short newMinorVersion = maxVersion == null ? (short)1 : (short)(maxVersion.MinorVersion + 1);

            AddNewVersionWindow newWindow = new AddNewVersionWindow(_softwarenumber, maxVersion.MajorVersion, newMinorVersion, ref _reg);
            newWindow.Show();
        }
    }
}
