﻿using SoftwareNumbers.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Windows.Input;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using JamzFun.Command;
using JamzFun.NPC;

namespace SoftwareNumbers.Model
{

    

    /// <summary>
    /// Base class for SN_ViewModel
    /// </summary>
    public class RegistryModel : NPC_ModelBase
    {
        private SoftwareNumbersModel _Model = new SoftwareNumbersModel();
        private ICollectionView _SWSR;
        private ICollectionView _SWSV;
        private ICollectionView _Users;

        public string newVersionDefaultDesc = "Software registered to database.";

        public RegistryModel()
        {
            _Model.SW_SR.Load();
            _SWSR = CollectionViewSource.GetDefaultView(_Model.SW_SR.Local);

            _Model.SW_SV.Load();
            _SWSV = CollectionViewSource.GetDefaultView(_Model.SW_SV.Local);
            _SWSV.CollectionChanged += _SWSV_CollectionChanged;

            _Model.User_List.Load();
            _Users = CollectionViewSource.GetDefaultView(_Model.User_List.Local);
        }

        private void _SWSV_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            _SWSR.Refresh();
        }

        /// <summary>
        /// Retrieve SW_SR from Registry
        /// </summary>             
        public ICollectionView GetNumbers
        {
            get
            {
                //this._Model.SW_SR.Load();
                //return CollectionViewSource.GetDefaultView(_Model.SW_SR.Local);
                return _SWSR;
            }
        }

        /// <summary>
        /// Retrieve SW_SV from Registry based on softwareNumber
        /// </summary>
        public ICollectionView GetVersions
        {
            get
            {
                //this._Model.SW_SV.Load();
                //return CollectionViewSource.GetDefaultView(_Model.SW_SV.Local);
                return _SWSV;
            }
        }

        public ObservableCollection<User_List> GetUserList
        {
            get
            {
                this._Model.User_List.Load();
                return this._Model.User_List.Local;
            }
        }

        public bool VersionExists(short softwarenumber, short majorVersion,short minorVersion)
        {
            return (_Model.SW_SV.Local.Where(x => (x.Version.MajorVersion == majorVersion) && (x.Version.MinorVersion == minorVersion)).Count() > 0);
        }

        

        /// <summary>
        /// Save Everything
        /// </summary>      
        private void _Save()
        {
            try
            {
                Console.Write(_Model.SaveChanges());
            }
            catch(ArgumentException e)
            {
                MessageBox.Show(e.Message,"Uh oh",MessageBoxButton.OK,MessageBoxImage.Exclamation);
            }
        }
        public DelegateCommand Save { get { return new DelegateCommand(_Save); } }
        

        private void _FilterNumbers(object parameter)
        {
            string mystring = (string)parameter;
            //Console.WriteLine(mystring);

            GetNumbers.Filter = item =>
            {
                SW_SR record = item as SW_SR;
                string tmpnumber = record.SR_SoftwareNumber.ToString("000");
                string tmpEnumber = "e" + tmpnumber;
                
                return ((record.SR_Description.Contains(mystring))
                            || (mystring == tmpEnumber) 
                            || record.SR_SoftwareNumber.ToString().Contains(mystring));
            };

        }
        public DelegateCommand FilterNumbersCommand { get { return new DelegateCommand(_FilterNumbers); } }
        public void FilterNumbers(string textString)
        {
            _FilterNumbers(textString);
        }

  

        /// <summary>
        /// Add a new software number entry
        /// </summary>
        /// <param name="newSWSR"></param>
        public void AddNewSWSR(SW_SR newSWSR)
        {
            this._Model.SW_SR.Load();
            this._Model.SW_SR.Add(newSWSR);
            this._Save();
            this._Model.SW_SR.Load();

            // create a new 0v0 version for this software
            SW_SV newSWSV = new SW_SV();
            newSWSV.Version.MajorVersion = 0;
            newSWSV.Version.MinorVersion = 0;
            newSWSV.SV_SoftwareNumber = newSWSR.SR_SoftwareNumber; //because amazingly, once you add this to the model, it picks up the software number!
            newSWSV.SV_ReleaseDate = DateTime.Now;
            newSWSV.ChangeLog = newVersionDefaultDesc;
            newSWSV.ENG0 = (from x in _Users.OfType<User_List>() where (x.login == Environment.UserName) select x.ID).FirstOrDefault();
            AddNewSWSV(newSWSV);
        }

        public void AddNewSWSV(SW_SV newSWSV)
        {
            this._Model.SW_SV.Load();
            this._Model.SW_SV.Add(newSWSV);
            this._Save();
            this._Model.SW_SV.Load();
        }

        public Data.Version GetMaxVersion(short softwarenumber)
        {
            IEnumerable<SW_SV> versions = _Model.SW_SV.Local.Where(x => x.SV_SoftwareNumber == softwarenumber);

            if (versions.Count() != 0)
            {
                SW_SV result = (versions.Aggregate((h, n) => h.Version.MajorVersion > n.Version.MajorVersion ? h : n));
                return result.Version;
            }
            else
            {
                return null;
            }
        }
    }


    //public class VersionsModel
    //{

    //    private RegistryModel _reg;
    //    private ObservableCollection<SW_SV> _versionSet;

    //    public short SoftwareNumber { get; }

    //    public VersionsModel(ref RegistryModel registryContext, short softwareNumber = 0)
    //    {
    //        this._reg = registryContext;
    //        this.SoftwareNumber = softwareNumber;
    //        if (softwareNumber != 0)   // use ZERO do denote all of them
    //        {
    //            _versionSet = new ObservableCollection<SW_SV>(_reg.GetVersions.Where(t => t.SV_SoftwareNumber == SoftwareNumber));
    //        }
    //        else
    //        {
    //            _versionSet = _reg.GetVersions;
    //        }

    //    }

    //    /// <summary>
    //    /// get versions based on this model's software number
    //    /// </summary>
    //    public ObservableCollection<Data.SW_SV> VersionsList
    //    {
    //        get
    //        {
    //            return new ObservableCollection<SW_SV>(_reg.GetVersions.Where(t => t.SV_SoftwareNumber == SoftwareNumber));
    //            //return _reg.getVersions.TakeWhile(t => t.SV_SoftwareNumber == _softwareNumber);
    //        }
    //    }

    //    public Data.Version MaxVersion
    //    {
    //        get
    //        {
    //            if (_versionSet.Count != 0)
    //            {
    //                SW_SV result = (_versionSet.Aggregate((h, n) => h.Version.MajorVersion > n.Version.MajorVersion ? h : n));
    //                return result.Version ?? new Data.Version((short)0, (short)0, (short)0);
    //            }
    //            else
    //            {
    //                return new Data.Version((short)0, (short)0, (short)0);
    //            }           
    //        }
    //    }

    //    public bool VersionExists(short majorVersion, short minorVersion)
    //    {
    //        IEnumerable<SW_SV> result = _versionSet.Where(v => (v.Version.MajorVersion == majorVersion) && (v.Version.MinorVersion == minorVersion));

    //        if (result.Count() != 0) { return true; }
    //            else { return false; }

    //    }

    //    public void addNewVersion(SW_SV newSWSV)
    //    {
    //        _reg.AddNewSWSV(newSWSV);
    //    }

    //}


    /******************/
    #region UsersModelRegion
    public class UsersModel
    {
        private ObservableCollection<User_List> _user_List;

        public UsersModel(RegistryModel registryContext)
        {
            _user_List = registryContext.GetUserList;
        }

        public User_List GetSpecificUser(string loginName)
        {
            return _user_List.Where(x => x.login == loginName).FirstOrDefault();
        }

        public ObservableCollection<User_List> GetSpecificDepartment(short departmentNumber)
        {
            return new ObservableCollection<User_List>(_user_List.Where(x => x.department == departmentNumber));
        }

    } 

    #endregion

}
