﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace JamzFun.NPC
{
    public class NPC_ModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Property Changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise change event - uses CallerMemberName so just call it with:
        /// RaisePropertyChanged();
        /// </summary>
        /// <param name="propertyName"></param>
        internal void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// <c>setProp(value, prop, set) </c>
        /// <para>The purpose of which is to reduce boilerplate for setting object properties, even for other objects instantiated inside this base object.</para>
        /// <para>This also calls RaisePropertyChanged.</para>
        /// </summary>
        /// <param name="value"> New value to check.</param>
        /// <param name="prop"> Old value to check against.</param>
        /// <param name="Set"> Usage example: newValue => "obj.property" = newValue</param>
        /// <returns name="bool"> Returns true if property was set.</returns>
        /// <remarks> Obviously, the "set" parameter being a lambda function, any other action can be applied to this.</remarks>
        internal bool SetProp<T> (T value, T prop, Action<T> Set, [CallerMemberName] string propertyCaller = null)
        {
            if (!EqualityComparer<T>.Default.Equals(value, prop))
            {
                Set(value);
                RaisePropertyChanged(propertyCaller);
                return true;
            }
            else return false;
        }
    }
}


namespace JamzFun.Command
{
    /// <summary>
    /// Base class to implement the ICommand interface
    /// </summary>
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _action;
        private readonly Action _dullAction;
        private int type = 0;
        
        private Action _Save;

        const int DULL = 0;
        const int NORMAL = 1;

        public DelegateCommand(Action<object> action) { _action = action; type = NORMAL; }

        public DelegateCommand(Action dullAction) { _dullAction = dullAction; type = DULL; }
        
        public void Execute(object parameter)
        {
            if (type == NORMAL)
            {
                _action(parameter);
            }
            else if (type == DULL)
            {
                _dullAction();
            }
                
        }

        public bool CanExecute(object parameter) { return true; }

#pragma warning disable 67
        public event EventHandler CanExecuteChanged;
#pragma warning restore 67
    }



}
