﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SoftwareNumbers.Model;
using SoftwareNumbers.Data;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Data;
using SoftwareNumbers.Helpers;

namespace SoftwareNumbers
{
    /// <summary>
    /// Interaction logic for AddNewVersionWindow.xaml
    /// </summary>
    public partial class AddNewVersionWindow : Window
    {
        private RegistryModel _reg;
        private short _softwarenumber;
        private ObservableCollection<User_List> _engiesCollection;
        private UsersModel users;
        private List<string> _engies;
         
        public AddNewVersionWindow(short softwarenumber, short majorVersion, short minorVersion, ref RegistryModel reg)
        {            
            _reg = reg;
            _softwarenumber = softwarenumber;

            InitializeComponent();

            // some pre-filleds
            MajorVersionTextBox.Text = Convert.ToString(majorVersion);
            MinorVersionTextBox.Text = Convert.ToString(minorVersion);
            SVDate.Text = DateTime.Now.ToString();

            users = new UsersModel(reg);
            _engiesCollection = users.GetSpecificDepartment(ConfigVariables.engineeringDepartment);
            _engies = new List<string>(_engiesCollection.Select(x => x.longname));
            eng0.ItemsSource = eng1.ItemsSource = eng2.ItemsSource = _engies;

            ApplicationUser thisUser = new ApplicationUser(_reg);
            eng0.Text = thisUser.LongName;

        }

        private void ValidateVersionTextBox(object sender, TextChangedEventArgs e)
        {
            if (this.IsVisible == false) return;

            short major = short.Parse(MajorVersionTextBox.Text ?? "0");
            short minor = short.Parse(MinorVersionTextBox.Text ?? "0");
            TextBox tb = (TextBox)sender;

            if (_reg.VersionExists(_softwarenumber,(short)major, (short)minor))
            {
                Dispatcher.BeginInvoke(new Action(() => tb.Undo()));
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO: do a round-robin check for changed items
            this.Close();
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            SW_SV newRecord = new SW_SV();
            short major = short.Parse(MajorVersionTextBox.Text ?? "0");
            short minor = short.Parse(MinorVersionTextBox.Text ?? "0");

            newRecord.Version.MajorVersion = major;
            newRecord.Version.MinorVersion = minor;
            newRecord.SV_SoftwareNumber = _softwarenumber;
            newRecord.SV_ReleaseDate = Convert.ToDateTime(SVDate.Text);
            newRecord.ChangeLog = DescriptionBox.Text;
            newRecord.ENG0 = GetThisUser(eng0.Text);
            newRecord.ENG1 = GetThisUser(eng1.Text);
            newRecord.ENG2 = GetThisUser(eng2.Text);

            _reg.AddNewSWSV(newRecord);
            this.Close();
        }

        private short? GetThisUser(string userString)
        {
            if ((userString == null) || (userString.Length == 0))
            {
                return null;
            }
            else
            { 
                return (_engiesCollection.Where(x => x.longname == userString)).FirstOrDefault().ID;
            }
        }

    }
}
