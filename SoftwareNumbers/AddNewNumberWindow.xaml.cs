﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Shapes;
using SoftwareNumbers.Data;

namespace SoftwareNumbers.Model
{

    /// <summary>
    /// Interaction logic for AddNewWindow.xaml
    /// </summary>
    public partial class AddNewNumberWindow : Window
    {
        private RegistryModel _registry;
        
        public AddNewNumberWindow(ref RegistryModel registryContext)
        {
            InitializeComponent();
            DataContext = registryContext;
            _registry = registryContext;
            textbox_newDesc.Text = ConfigVariables.NEWDESC_DEFAULT;
        }

        private void submitNew()
        {
            if (textbox_newDesc.Text != ConfigVariables.NEWDESC_DEFAULT)
            {
                MessageBoxResult result = MessageBox.Show("Add new entry?", "Add New", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    SW_SR new_SWNo = new SW_SR();
                    new_SWNo.SR_Description = textbox_newDesc.Text;
                    new_SWNo.SR_CreationDate = DateTime.Now;
                    _registry.AddNewSWSR(new_SWNo);
                    this.Close();
                }
            }
        }

        private void textbox_newDesc_GotFocus(object sender, RoutedEventArgs e)
        {
            if (textbox_newDesc.Text == ConfigVariables.NEWDESC_DEFAULT)
            {
                textbox_newDesc.Text = "";
            }
        }

        private void textbox_newDesc_LostFocus(object sender, RoutedEventArgs e)
        {
            if (textbox_newDesc.Text == "")
            {
                textbox_newDesc.Text = ConfigVariables.NEWDESC_DEFAULT;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (textbox_newDesc.Text != ConfigVariables.NEWDESC_DEFAULT)
            {
                MessageBoxResult result = MessageBox.Show("Cancel new software entry?", "Close dialog", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            submitNew();
        }

        private void textbox_newDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                submitNew();
            }
        }
    }
}
